<!--?php /* Template Name: Home */ ?-->
<?php get_header(); ?>

	<main role="main">
		<!-- section -->
        <section class="intro">
            <div class="container">

                <h2><?php the_title(); ?></h2>

                <div class="row">

                    <div class="col-sm-6">
                        <?php if ( has_post_thumbnail() ) : ?>
                        <figure>
                            <?php the_post_thumbnail('full'); ?>
                        </figure>
                    <?php endif; ?>
                    </div>

                    <div class="col-sm-6">
                        <?php if (have_posts()): while (have_posts()) : the_post(); ?>

                        <!-- article -->
                        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                            <?php the_content(); ?>

                            <a href="<?php echo get_permalink(); ?>" class="btn"><?php echo the_field("know_more"); ?></a>

                        </article>
                        <!-- /article -->

                        <?php endwhile; ?>
                        
                        <?php endif; ?>
                        
                    </div>

                </div>
            </div>
        </section>
		<!-- /section -->
	</main>

    <section class="team">
        <div class="container">
            <div class="row">
                <h2>Meet the team</h2>
                <?php 

                   $the_query = new WP_Query( array(
                     'category_name' => 'team',
                      'posts_per_page' => 2,
                   )); 
                ?>

                <?php if ( $the_query->have_posts() ) : ?>
                  <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                    <div class="member col-sm-6">
                        <div class="row">
                            <div class="col-xs-4">
                                <?php if ( has_post_thumbnail() ) : ?>
                                    <figure>
                                        <?php the_post_thumbnail('full'); ?>
                                    </figure>
                                <?php endif; ?>
                            </div>
                            <div class="col-xs-8">
                                <h3><?php the_title(); ?></h3>
                                <p><?php the_excerpt(); ?></p>
                                <a href="<?php echo get_permalink(); ?>" class="btn-link">Read more</a>
                            </div>
                        </div>
                    </div>

                  <?php endwhile; ?>
                  <?php wp_reset_postdata(); ?>

                <?php else : ?>
                  <p><?php __('No Team'); ?></p>
                <?php endif; ?>
                
            </div>
        </div>
    </section>

<?php get_footer(); ?>
